## Spring Configuration Style

### WTF?
В неочевидных случаях используйте нативные XML комментарии или описание внутри `<description>`. **Например**:

~~~xml
<beans>
    <description>Конфигурация Jetty</description>
    <!-- ... продолжение -->
</beans>
~~~

### Не указывайте номера версий Spring Schema
Это не просто лишний код, но еще и дополнительная боль при миграции на новые версии Spring. По умолчанию используется наивысшая доступная версия схемы из зависимостей. **Например**:

~~~xml
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xmlns:context="http://www.springframework.org/schema/context"
  xsi:schemaLocation="http://www.springframework.org/schema/beans
  http://www.springframework.org/schema/beans/spring-beans.xsd
  http://www.springframework.org/schema/context/
  http://www.springframework.org/schema/context/spring-context.xsd">
  
  <!-- ... продолжение -->
    
</beans>
~~~

### DI через конструктор или через сеттеры?
Главный бонус DI через конструктор - потокобезопасность (нет сеттеров, объект immutable). К тому же, он будет сразу полностью инициализирован. Если параметров в конструкторе слишком много или часть параметров опциональны, этот способ конечно не подходит. **Например**:

~~~xml
<!-- DI здорового человека (через конструктор) -->
<bean id="exampleDao"   class="ru.thezeep.example.dao.ExampleDao">
    <constructor-arg ref="datasource"/>
</bean>
 
<!-- DI курильщика (через сеттер) -->
<bean id="exampleDao" class="ru.thezeep.example.dao.ExampleDao">
    <property name="datasource"  ref="datasource">
</bean>
~~~

### Сопоставление аргументов
Если не возникает неоднозначности, сопоставление с типом удобнее, чем использование индекса: лучше читается и избавляет от ошибок copy-paste. **Например**:

~~~xml
<!-- Плохо: -->
<bean id="exampleDao" class="ru.thezeep.example.dao.ExampleDao">
    <constructor-arg index="0" value="rest"/>
    <constructor-arg index="1" value="8080"/>
</bean>
 
<!-- Лучше: -->
<bean id="exampleDao" class="ru.thezeep.example.dao.ExampleDao">
    <constructor-arg type="java.lang.String" value="rest"/>
    <constructor-arg type="int" value="8080"/>
</bean>
~~~

### Меньше кода - меньше багов 
По возможности используйте сокращенную форму. **Например**:

~~~xml
<!-- Полная форма: -->
<bean id="exampleDao" class="ru.thezeep.example.dao.ExampleDao">
    <property name="datasource">
        <ref bean="datasource"></ref>
        <value>datasource</value>
     </property>
</bean>
 
<!-- Можно короче: -->
<bean id="exampleDao" class="ru.thezeep.example.dao.ExampleDao">
    <property name="datasource"  ref="datasource" value="datasource">
</bean>
~~~
Переиспользуйте определения, если того требует ситуация. **Например**:

~~~xml
<bean id="abstractDataSource" class="org.apache.commons.dbcp.BasicDataSource"
    destroy-method="close"
    p:driverClassName="${jdbc.driverClassName}"
    p:username="${jdbc.username}"
    p:password="${jdbc.password}" />
 
<bean id="concreteDataSourceOne" parent="abstractDataSource" p:url="${jdbc.databaseurlOne}"/>
<bean id="concreteDataSourceTwo" parent="abstractDataSource" p:url="${jdbc.databaseurlTwo}"/>
~~~

### И наконец - не давайте ЭТО администратору
Выносите настраиваемые свойства в отдельный файл. **Например**:

~~~xml
<context:property-placeholder location="classpath:persistence.properties"/>
~~~
